pipeline {
    agent {
	label 'master'
    }
    environment {
	JENKINS_TOKEN = credentials('jenkins_token')
    }

    options {
        disableConcurrentBuilds()
	buildDiscarder logRotator(daysToKeepStr: '50', numToKeepStr: '300')
    }

    parameters{
        string name: 'name', defaultValue: 'manually_started', description: 'this parameter is set to the relevant commit when builds are triggered'
        string name: 'type', defaultValue: 'percheckin', description: 'developer/daily/percheckin/release type specified to the test harnesses'
        string name: 'revision', defaultValue: '', description: 'eg. mesa=<sha1> piglit=<sha1> ...'
        string name: 'build_support_branch', defaultValue: 'origin/master', description: 'specifies the branch of mesa_jenkins scripts used for the build'
        string name: 'notify', defaultValue: 'mark.janes@intel.com'
        booleanParam name: 'rebuild', defaultValue: false, description: 'Rebuild all components?'
    }
    stages {
	stage ('setup') {
	    steps {
		script {
		    currentBuild.displayName = "${BUILD_NUMBER}-${params.name}-${params.type}"
		}

	        checkout([$class: 'GitSCM', branches: [[name: "${params.build_support_branch}" ]],
			  userRemoteConfigs: [[name: 'origin', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/origin'],
					      [name: 'majanes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/majanes'],
					      [name: 'ybogdano', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ybogdano'],
					      [name: 'ngcortes', url: 'git://otc-mesa-ci.local/git/mirror/mesa_jenkins/ngcortes']]])
	    }
	}
	stage('clear stale testing info') {
            steps  {
		sh 'rm -rf results/test'
                sh 'rm -rf summary.xml'
                sh 'rm -rf test_summary.txt'
	    }
	}
	stage('fetch sources') {
	    steps {
		sh "python3 -u fetch_sources.py --branch=dev_janesma --project=mesa ${params.revision}"
	    }
	}
	stage('printenv') {
	    steps {
		sh 'env'
	    }
	}


	stage('execute test suites') {
	    steps {
		sh "python3 -u repos/mesa_ci/scripts/build_jenkins.py --project=mesa --branch=dev_janesma"
	    }
	}

	stage('send email alert') {
	    steps {
	    mail to: params.notify,
		subject: "Mesa CI completed with status ${currentBuild.result}: $JOB_BASE_NAME ${currentBuild.displayName}",
		body: """See ${BUILD_URL}
Results at https://mesa-ci.01.org/${JOB_BASE_NAME}/builds/${BUILD_NUMBER}/group/63a9f0ea7bb98050796b649e85481845"""
	    }
	}
    }
    // try {
    // 	withCredentials([file(credentialsId: 'jenkins_token', variable: 'jenkins_token')]) {
    // 	    stage('publish results') {
    // 		archiveArtifacts allowEmptyArchive: true, artifacts: "summary.xml,results/test/stripped/piglit*xml,results/test/card_error*,results/test/core.*"
    // 		sh "JENKINS_TOKEN=${jenkins_token} python3 -u repos/mesa_ci/scripts/trigger_sql_import.py"
    // 		junit allowEmptyResults: true, testResults: "results/test/stripped/piglit*xml"
    // 		step([$class: 'ACIPluginPublisher', name: 'summary.xml,results/test/card_error*', shownOnProjectPage: false])
    // 	    }
    // 	}
    // }
    // catch (error) {
    // 	echo "ERROR: could not publish results: ${error}"
    // }
}

